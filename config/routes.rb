Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'tickets#index'

  resources :tickets, only: %i[index show]

  namespace :api do
    namespace :v1 do
      resources :tickets, only: :create
    end
  end
end
