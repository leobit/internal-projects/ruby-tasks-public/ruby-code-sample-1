describe Address, type: :model do
  subject { Address.new }

  it { should belong_to(:excavator) }
  it { should validate_presence_of(:address) }
  it { should validate_presence_of(:city) }
  it { should validate_presence_of(:state) }
end
