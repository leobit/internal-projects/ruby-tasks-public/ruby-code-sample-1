describe Excavator, type: :model do
  subject { Excavator.new }

  it { should belong_to(:ticket) }
  it { should have_one(:address) }
  it { should validate_presence_of(:company_name) }
  it { should validate_presence_of(:ticket) }
  it { should accept_nested_attributes_for(:address) }
end
