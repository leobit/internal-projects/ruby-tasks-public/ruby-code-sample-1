describe Ticket, type: :model do
  subject { described_class.new }

  it { should have_one(:excavator) }
  it { should validate_presence_of(:request_number) }
  it { should validate_presence_of(:sequence_number) }
  it { should validate_presence_of(:request_type) }
  it { should accept_nested_attributes_for(:excavator) }
end
