describe TicketsController, type: :controller do
  describe '#index' do
    let!(:tickets) { create_list(:ticket, 5) }

    before { get :index }

    it { expect(response).to have_http_status(:ok) }
    it { expect(response).to render_template(:index) }
    it do
      expect(@controller.instance_variable_get(:@collection)).to eq(tickets)
    end
  end

  describe '#show' do
    context 'valid' do
      let!(:ticket) { create(:ticket) }

      before { get :show, params: { id: ticket.id } }

      it { expect(response).to have_http_status(:ok) }
      it { expect(response).to render_template(:show) }
      it do
        expect(@controller.instance_variable_get(:@ticket)).to eq(ticket)
      end
    end

    context 'invalid' do
      before { get :show, params: { id: 0 } }

      it { expect(response).to have_http_status(:not_found) }
    end
  end
end
