describe Api::V1::TicketsController, type: :controller do
  describe '#create.json' do
    let(:params) { build(:api_call) }

    context 'valid' do
      before { post :create, params: params, as: :json }

      it { expect(response).to render_template :show }
    end

    context 'invalid' do
      before { post :create, params: {}, as: :json }

      it { expect(response).to render_template :errors }
    end
  end
end
