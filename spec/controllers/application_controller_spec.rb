describe ApplicationController, type: :controller do
  it do
    should rescue_from(ActiveRecord::RecordNotFound).with(:resource_not_found)
  end
end
