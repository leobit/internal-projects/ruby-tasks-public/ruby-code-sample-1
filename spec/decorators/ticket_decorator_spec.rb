describe TicketDecorator do
  let(:resource) { build(:ticket, :with_excavator, :with_polygon) }

  subject { resource.decorate }

  describe 'associations' do
    it { expect(subject.excavator).to be_decorated_with(ExcavatorDecorator) }
  end

  describe '#due_time' do
    let(:ticket)         { build(:ticket, response_due_time: nil) }
    let(:decorator)      { ticket.decorate }
    let(:formatted_date) { I18n.l(resource.response_due_time, format: :long) }

    it { expect(subject.due_time).to   eq(formatted_date) }
    it { expect(decorator.due_time).to be_nil }
  end

  describe '#service_areas' do
    let(:service_areas) { [resource.primary_service_area, *resource.additional_service_areas] }
    let(:ticket)        { build(:ticket, primary_service_area: nil, additional_service_areas: nil) }
    let(:decorator)     { ticket.decorate }

    it { expect(subject.service_areas).to   match(service_areas) }
    it { expect(decorator.service_areas).to be_empty }
  end
end
