describe AddressDecorator do
  let(:resource) { build(:address) }

  subject { resource.decorate }

  describe '#address_line' do
    let(:address)      { resource.address }
    let(:city)         { resource.city }
    let(:zip_code)     { resource.zip_code }
    let(:state)        { resource.state }
    let(:full_address) { [address, zip_code, city, state].join(', ') }

    it { expect(subject.full_address).to eq(full_address) }
  end
end
