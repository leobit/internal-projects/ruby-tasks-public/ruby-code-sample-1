FactoryBot.define do
  factory :ticket do
    request_number { Faker::Code.imei }
    sequence_number { Faker::Number.number(4).to_i }
    request_type { Ticket.request_types.values.sample }
    primary_service_area { Faker::Hipster.word }
    additional_service_areas { Faker::Hipster.words(4) }
    response_due_time { Faker::Time.between(1.days.ago, 1.year.from_now) }

    trait :with_polygon do
      polygon 'POLYGON ((25.774 -80.190, 18.466 -66.118, 32.321 -64.757, 25.774 -80.190))'
    end

    trait :with_excavator do
      association :excavator, factory: %i[excavator with_address]
    end
  end
end
