class CreateTickets < ActiveRecord::Migration[5.2]
  def change
    create_table :tickets do |t|
      t.string     :request_number
      t.integer    :sequence_number
      t.string     :request_type
      t.string     :primary_service_area
      t.string     :additional_service_areas, array: true, default: [], null: false
      t.datetime   :response_due_time
      t.st_polygon :polygon
    end

    add_index :tickets, :additional_service_areas, using: :gin
    add_index :tickets, :polygon,                  using: :gist
  end
end
