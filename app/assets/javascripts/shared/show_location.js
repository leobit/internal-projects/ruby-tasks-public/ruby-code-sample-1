//= require shared/borders.js

class ShowLocation {
  static get SELECTOR()       { return '#map' }
  static get DEFAULT_ZOOM()   { return 5 }
  static get MAP_TYPE_ID()    { return 'terrain' }
  static get BORDER_COLOR()   { return '#00ff21' }
  static get COLOR()          { return '#00ff21' }
  static get STROKE_OPACITY() { return 0.7 }
  static get STROKE_WEIGHT()  { return 2.5 }
  static get FILL_OPACITY()   { return 0.5 }

  constructor() {
    this.$map        = $(ShowLocation.SELECTOR);
    this.coordinates = this.$map.data('coordinates');
  }

  init() {
    if (this.$map.length > 0) {
      this.transformCoordinates(this);
      this.drawPolygon(this);
    }
  }

  transformCoordinates(self) {
    self.coordinates = self.coordinates.map(function(line) {
      return line.map((coordinate) => {
        return {
          lat: coordinate[0],
          lng: coordinate[1]
        }
      })
    })
  }

  drawPolygon(self) {
    let polygon = new google.maps.Polygon({
      paths:         self.coordinates,
      strokeColor:   ShowLocation.BORDER_COLOR,
      strokeOpacity: ShowLocation.STROKE_OPACITY,
      strokeWeight:  ShowLocation.STROKE_WEIGHT,
      fillColor:     ShowLocation.COLOR,
      fillOpacity:   ShowLocation.FILL_OPACITY
    });

    polygon.setMap(self.loadMap(self));
  }

  loadMap(self) {
    return new google.maps.Map(self.$map[0], {
      zoom:      ShowLocation.DEFAULT_ZOOM,
      center:    self.centerView(self.coordinates, self.defineBorders(self)),
      mapTypeId: ShowLocation.MAP_TYPE_ID
    });
  }

  defineBorders(self) {
    return new Borders(self.coordinates).init();
  }

  centerView(coordinates, borders) {
    return {
      lat: coordinates.length !== 0 ? (borders.minLat + borders.maxLat) / 2 : 0,
      lng: coordinates.length !== 0 ? (borders.minLng + borders.maxLng) / 2 : 0
    }
  }
}
