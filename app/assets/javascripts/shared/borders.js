class Borders {
  static get DEFAULT_MIN_COORDINATE() { return 9999999 }
  static get DEFAULT_MAX_COORDINATE() { return -9999999 }
  static get MAX_VALUE_TYPE()         { return 'max' }
  static get MIN_VALUE_TYPE()         { return 'min' }

  constructor(coordinates) {
    this.coordinates = coordinates;
    this.Borders  = undefined;
  }

  init() {
    this.reset(this);
    this.find(this);

    return this.Borders;
  }

  reset(self) {
    self.Borders = {
      maxLat: Borders.DEFAULT_MAX_COORDINATE,
      maxLng: Borders.DEFAULT_MAX_COORDINATE,
      minLat: Borders.DEFAULT_MIN_COORDINATE,
      minLng: Borders.DEFAULT_MIN_COORDINATE
    }
  };

  find(self) {
    self.coordinates.forEach(function(line) {
      let latValues = line.map(function(point) { return point.lat }),
        lngValues = line.map(function(point) { return point.lng }),
        maxLat    = Math.max(...latValues),
        maxLng    = Math.max(...lngValues),
        minLat    = Math.min(...latValues),
        minLng    = Math.min(...lngValues);

      self.replaceEdgePoint('maxLat', Borders.MAX_VALUE_TYPE, maxLat);
      self.replaceEdgePoint('maxLng', Borders.MAX_VALUE_TYPE, maxLng);
      self.replaceEdgePoint('minLat', Borders.MIN_VALUE_TYPE, minLat);
      self.replaceEdgePoint('minLng', Borders.MIN_VALUE_TYPE, minLng);
    })
  };

  replaceEdgePoint(key, valueType, value) {
    let comparison = valueType === 'max' ? this.Borders[key] < value : this.Borders[key] > value;

    if (comparison) {
      this.Borders[key] = value;
    }
  };
}
