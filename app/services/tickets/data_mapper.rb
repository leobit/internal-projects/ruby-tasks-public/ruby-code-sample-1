module Tickets
  class DataMapper < ApplicationService
    def initialize(params = {})
      @params = params
    end

    def call
      return unless root

      attributes
    end

    private

    attr_reader :params

    def attributes
      ticket_attributes.merge(excavator_attributes: excavator_attributes)
    end

    def ticket_attributes
      {
        request_number:           root.dig(:RequestNumber),
        sequence_number:          root.dig(:SequenceNumber),
        request_type:             root.dig(:RequestType),
        primary_service_area:     root.dig(:ServiceArea, :PrimaryServiceAreaCode, :SACode),
        additional_service_areas: root.dig(:ServiceArea, :AdditionalServiceAreaCodes, :SACode),
        response_due_time:        root.dig(:DateTimes, :ResponseDueDateTime),
        polygon:                  root.dig(:ExcavationInfo, :DigsiteInfo, :WellKnownText)
      }
    end

    def excavator_attributes
      {
        manned:             root.dig(:Excavator, :CrewOnsite),
        company_name:       root.dig(:Excavator, :CompanyName),
        address_attributes: address_attributes
      }
    end

    def address_attributes
      {
        address:  street_address,
        state:    address_info.dig(:State),
        city:     address_info.dig(:Place),
        zip_code: address_info.dig(:Zip)
      }
    end

    def street_address
      [
        address_info.dig(:Address, :AddressNum)&.join(' '),
        address_info.dig(:Street, :Prefix),
        address_info.dig(:Street, :Name),
        address_info.dig(:Street, :Type),
        address_info.dig(:Street, :Suffix)
      ].reject(&:blank?).join(' ')
    end

    def address_info
      @address_info ||= root.dig(:ExcavationInfo, :DigsiteInfo, :AddressInfo) || {}
    end

    def root
      @root ||= params.dig(:params)
    end
  end
end
