module Tickets
  class Factory < ApplicationService
    attr_reader :params

    def initialize(params = {})
      @params = params
    end

    def call
      Ticket.create(creation_params)
    end

    private

    def creation_params
      ::Tickets::DataMapper.call(params)
    end
  end
end
