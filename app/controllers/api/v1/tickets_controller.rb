module Api
  module V1
    class TicketsController < Api::ApplicationController
      def create
        @resource = ::Tickets::Factory.call(params: params)

        if @resource.errors.blank?
          render :show
        else
          render :errors
        end
      end
    end
  end
end
