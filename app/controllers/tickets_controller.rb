class TicketsController < ApplicationController
  PER_PAGE = 15

  decorates_assigned :ticket

  def index
    @collection = Ticket.all
                        .page(params[:page])
                        .per(PER_PAGE)
                        .decorate
  end

  def show
    @ticket = Ticket.find(params[:id])
  end
end
