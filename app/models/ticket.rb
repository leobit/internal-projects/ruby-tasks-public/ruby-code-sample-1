class Ticket < ApplicationRecord
  has_one :excavator, inverse_of: :ticket, dependent: :destroy
  NORMAL = 'Normal'.freeze
  URGENT = 'Urgent'.freeze

  enum request_type: {
    normal: NORMAL,
    urgent: URGENT
  }

  validates :request_number,  presence: true
  validates :sequence_number, presence: true
  validates :request_type,    presence: true

  accepts_nested_attributes_for :excavator
end
