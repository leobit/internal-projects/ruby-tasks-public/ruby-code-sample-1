class AddressDecorator < ApplicationDecorator
  def full_address
    [object.address, zip_code, city, state].reject(&:blank?).join(', ')
  end
end
