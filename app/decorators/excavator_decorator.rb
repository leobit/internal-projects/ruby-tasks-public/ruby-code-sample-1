class ExcavatorDecorator < ApplicationDecorator
  decorates_association :address

  def manned_text
    manned? ? I18n.t('generic.yes') : I18n.t('generic.no')
  end
end
