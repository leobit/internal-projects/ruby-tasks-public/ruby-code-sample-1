class TicketDecorator < ApplicationDecorator
  decorates_association :excavator

  delegate :company_name, :manned_text, :id, :address,
           to: :excavator, prefix: true, allow_nil: true

  delegate :full_address,
           to: :excavator_address, allow_nil: true

  delegate :coordinates,
           to: :polygon, prefix: true, allow_nil: true

  def due_time
    I18n.l(response_due_time, format: :long) if response_due_time
  end

  def service_areas
    [primary_service_area, *additional_service_areas].compact
  end

  def serialized_coordinates
    polygon_coordinates.to_json
  end
end
